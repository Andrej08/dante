#!/usr/bin/env python

import string, random, sys, os, re, imp
cp = imp.find_module('cPickle')
cPickle = imp.load_module('cPickle', cp[0], cp[1], cp[2])

MAXGEN=1000 # Max number of words to generate
NONWORD = '\n' # Use as start- and endmarkers
TARGET = '<TARGET>'
OTHER  = '<OTHER>'

CHATTINESS = 0.02

SAVECOUNT = 30

class MarkovChainer:
    def __init__(self, bot):
        self.bot = bot
        self.bot.register_handler("pubmsg", self.handle_pubmsg)
        self.bot.register_handler("join", self.handle_join)
        self.dict = cPickle.load(open("markov.state", 'r'))
        self.savecounter = 0

    def handle_join(self, connection, event):
        print "join: ", event.arguments()

    def replace_smart(self, word):
        if string.lower(word) == "who":
            return OTHER

        return word

    def handle_pubmsg(self, connection, event):

#self.clean_dict()
        spoken = 0
        addressed = 0
        directly_addressed = 0
        chainable = 1
        whonick = self.bot.nm_to_n(event.source())
        message = event.arguments()[0]
        target = event.target()
        if re.search(string.lower(self.bot._nickname),
                     string.lower(message)):
            addressed = 1

        if message == "%s, status?" % self.bot._nickname:
            connection.privmsg(target, "I know %s phrases" %
                               str(len(self.dict.keys())))
            return


#message = self.strip_nick(message, whonick)
        message = self.strip_shit(message)

        if chainable and whonick != "hugo":
            print "Learning"
            self.input(message)


        if not spoken:
            splitmsg = string.split(message, ' ')
            try:
                if len(splitmsg) >= 1 and \
                   string.strip(splitmsg[0], ",:") == self.bot.config.NICKNAME:
                   splitmsg.pop(0)
                   directly_addressed = 1 

                if len(splitmsg) >= 2:
                    text = string.strip(self.output(whonick, \
                                self.replace_smart(self.replace_name(splitmsg[0])),   \
                                self.replace_smart(self.replace_name(splitmsg[1]))))

                tries = 10

                while text == string.join(splitmsg) and tries > 0:
                    print "Found equal, retrying", tries, text
                    text = string.strip(self.output(whonick, 
                                self.replace_smart(self.replace_name(splitmsg[0])),
                                self.replace_smart(self.replace_name(splitmsg[len(splitmsg)-1]))))
                    print "New: ", text
                    tries = tries - 1
                    if tries == 0:
                        text = string.strip(self.output(whonick))
            except:
                text = string.strip(self.output(whonick))

        if directly_addressed \
            and not text.startswith(".") \
            and not text.startswith(whonick) \
            and not text.startswith(":") \
            and not whonick == "hugo":
            connection.privmsg(target, whonick + ", " + text)
        elif addressed or random.random() < CHATTINESS:
            connection.privmsg(target, text)

    def strip_nick(self, message, sender):
        return string.replace(string.strip(message),
                              self.bot._nickname, sender)

    def strip_shit(self, message):
        index = 0 
        for char in string.lower(message):
            if char in [',']:
                index = index + 1
            else:
                break
        return string.strip(message[index:])

        
    def input(self, originalText):
        word1, word2 = NONWORD, NONWORD
        wordList = string.split(originalText)
        for word3 in wordList: # Loop over rest of words

            word3 = self.replace_name(word3)

            if not self.dict.has_key( (word1,word2) ):
                self.dict[(word1,word2)] = [] #initialize to empty list
            self.dict[(word1, word2)].append(word3) # Add suffix for word-pair
            word1,word2 = word2, word3 # Shift in new words as dictionary keys
        self.dict[(word1,word2)] = [NONWORD] # Mark end of text
        self.savecounter = self.savecounter + 1        
        if self.savecounter >= SAVECOUNT:
            self.dumpdb()
            self.savecounter = 0

#print "debug: ", self.dict[("I", "am")]
            
    def dumpdb(self):
        try:
            os.rename("markov.state.2", "markov.state.3")
        except:
            pass
        try:
            os.rename("markov.state.1", "markov.state.2")
        except:
            pass
        try:
            os.rename("markov.state", "markov.state.1")
        except:
            pass
        cPickle.dump(self.dict, open("markov.state", "w"))

    def replace_mark(self, word, speaker = NONWORD):
        if word == TARGET:
            print "target replaced with", speaker
            return speaker
        elif word == OTHER:
            r = random.choice(self.bot.interfaces["users"]())
            print "other replaced with", r
            return r
        else:
            return word

    def replace_name(self, word):
        stripped = string.lower(string.strip(word, ",:><|?!.()\\/{}[]"))

        if stripped == string.lower(self.bot.config.NICKNAME):
            print "myself replaced with mark"
            return TARGET
        elif stripped in self.bot.interfaces["users"]():
            print "name, replaced with mark"
            return OTHER
            
        return word

    def output(self, speaker, word1=NONWORD, word2=NONWORD):
        #word1,word2 = NONWORD, NONWORD # Start at beginning

        output = self.replace_mark(word1, speaker) + " " + \
                 self.replace_mark(word2, speaker)

        try:
            for i in range(MAXGEN):
                successorList = self.dict[(word1,word2)]
                word3 = random.choice(successorList)
                if word3 == NONWORD:
                    break
                output = output + " "  + self.replace_mark(word3, speaker)

                word1, word2 = word2, word3
        except:
            return self.output(speaker)
            pass

        return output

    def clean_dict ( self ):
        for key in self.dict.keys():
            word1 = key[0]
            word2 = key[1]
            if word1 in self.bot.interfaces["users"]() or \
               word2 in self.bot.interfaces["users"]() or \
                len(self.dict[(word1, word2)]) == 0:
                try:
                    del self.dict[(word1, word2)]
                    print "removed (", string.strip(word1), ", ", string.strip(word2), ")"
                except:
                    print     "error: ", sys.exc_info()[0]
                    pass
                
            else:
                for name in self.bot.interfaces["users"]():
                    try:
                        self.dict[(word1,word2)].remove(name)
                        print "removed (", string.strip(word1), ", ", \
                                string.strip(word2), ").", string.strip(name)
                    except:
                        pass

        
    
