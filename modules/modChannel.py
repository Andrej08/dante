#!/usr/bin/env python

import string

class ChannelManager:
    """ this module performs channel maintenance functions """
    def __init__(self, bot):
        self.bot = bot
        self.bot.register_handler("kick", self.handle_kick)

        self.bot.register_handler("kick", self.handle_user_left)
        self.bot.register_handler("part", self.handle_user_left)
        self.bot.register_handler("quit", self.handle_user_left)

        self.bot.register_handler("join", self.handle_join)
        self.bot.register_handler("welcome", self.handle_welcome)
        self.bot.register_handler("namreply", self.handle_namreply)
        self.users = []

        self.bot.provide_interface("users", self.user_interface)
    
    def handle_namreply(self, connection,event):
        for name in string.split(event.arguments()[2], ' '):
            if name != self.bot.config.NICKNAME:
                self.users.append(name)

    def user_interface(self):
        return self.users

    def handle_user_left(self, connection, event):
        whonick=self.bot.nm_to_n(event.source())
        
        if whonick != self.bot.config.NICKNAME:
            print "Removed", whonick
            self.users.remove(whonick)

    def handle_welcome(self, connection, event):
        """ join channels when we connect to the server """
        for channel in self.bot.config.CHANNELS:
            connection.join(channel)

    def handle_kick(self,connection,event):
        """ rejoin on kick """
        whonick=self.bot.nm_to_n(event.source())
        self.users.remove(whonick)
        userhost=self.bot.nm_to_uh(event.source())
        message=event.arguments()[0]
        channel=event.target()
        victim=event.arguments()[0]
        reason=event.arguments()[1]
        if victim == self.bot.config.NICKNAME:
            connection.join(channel)

    def handle_join(self, connection, event):
        """ perform actions when users join """
        whonick=self.bot.nm_to_n(event.source())

        if whonick != self.bot.config.NICKNAME:
            print "Added", whonick
            self.users.append(whonick)
        else:
#            connection.who(event.target())
            print "target", event.target(), "source", event.source()
#connection.who(

        try:
            print self.bot.interfaces["get_user"](whonick).__dict__
        except:
            pass
        if whonick == self.bot.config.NICKNAME:
            # self joining
            pass

